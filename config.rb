require 'middleman-target'

activate :automatic_image_sizes
activate :directory_indexes
activate :target do |t|
  t.build_targets = {
    'phonegap' => {
      :includes => %w[android ios]
    },
    'android' => {
      :includes => %w[android]
    },
    'ios' => {
      :includes => %w[ios]
    }
  }
end

# Default directories
set :css_dir, 'css'
set :js_dir, 'js'
set :images_dir, 'img'

# Sprockets supports Bower, so we can add Bower components path directly:
sprockets.append_path File.join root, 'vendor', 'bower_components'
sprockets.append_path File.join root, 'vendor', 'css'
sprockets.append_path File.join root, 'vendor', 'js'

# Ignore non-root coffee and sass files
ignore 'js/src/*'
ignore 'css/src/*'

# Set output directory according to build target
case build_target
when :android
  set :build_dir, 'platforms/android/assets/www'
else
  set :build_dir, 'www' 
end

# internationalization
activate :i18n

# Development-specific configuration
configure :development do
  # Reload on assets change
  # activate :livereload
end

# Build-specific configuration
configure :build do
  # For example, change the Compass output style for deployment
  activate :minify_css

  # Minify Javascript on build
  activate :minify_javascript

  # Use relative URLs
  activate :relative_assets
end
