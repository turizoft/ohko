## Models

## View-Models
#= require src/view_models/shared/navigation

## Application
#= require src/application/router
#= require src/application/shared
#= require src/application/navigation

# Application entry point
class window.Application extends ohko.Application
  server_url: 'http://handshake-server.herokuapp.com'
  access_token_name: 'app_access_token'

  start: ->  
    # Start up application
    setTimeout @initialize_application, 0

  # Initialize application
  initialize_application: =>
    # Create session objects
    @session = new ohko.Session()
    @current_user = new ohko.Profile()

    # Create router
    @router = new Router()

    # Check if session can be restored before routing
    @session.try_to_restore_from_storage()

    # Initialize navigation
    @navigation = new Navigation()
    @navigation.set 'search_box_selector', '.search .edit'
    @navigation_vm = new NavigationViewModel()
    ko.applyBindings(@navigation_vm, $('.navigation')[0])
    ko.applyBindings(@navigation_vm, $('.left-off-canvas-menu')[0])

    # Initialize router
    Backbone.history.start({pushState: false})
    #@router.navigate 'home', trigger: true
