class window.Navigation extends ohko.Navigation
  hide_header_for: [
    'sessions_new'
    'sessions_destroy'
    'home_settings'
    'users_new'
    'passwords_new'
    'passwords_show'
  ]

  hide_footer_for: ['all']

  hide_floating_button_for: [
    'sessions_new'
    'sessions_destroy'
    'search_index'
    'home_settings'
    'users_new'
    'passwords_new'
    'passwords_show'
  ]

  keep_search_bar_for: [
    'search_index'
  ]