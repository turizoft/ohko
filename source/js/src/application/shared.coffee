# Application shared procedures

$ ->
  # Initialize frameworks
  call_frameworks()

  # NProgress
  NProgress.configure({ showSpinner: false })

  # Open close menu with swipe
  Hammer(document.body).on 'swiperight', (e) ->
    if e.center.x - e.deltaX < 32
      togglePushy() if not $('body').hasClass 'pushy-active'

  Hammer(document.body).on 'swipeleft', (e) ->
    togglePushy() if $('body').hasClass 'pushy-active'

  # Fastclick
  FastClick.attach document.body

  # Prevent reloading browser with empty links
  #$(document).on 'click', '[href^=#], .active a', (e) -> e.preventDefault()

  # Backbone links
  # $(document).on 'click', "a[href^='#'][data-bypass]", (e) ->
  #   e.preventDefault()
    # href = $(@).attr('href')
    # protocol = "#{@protocol}//"

    # if href.slice(protocol.length) isnt protocol
    #   e.preventDefault()
    #   console.log 'a'
      #dashboard_router.navigate(href, true)

  # $(window).on 'resize', ->
  #   h = $('.search-header').css('height')
  #   $('.results').css('top', h)

# Methods

# Start up libraries
window.call_frameworks = (element) ->
  

# Show an alert
window.show_alert = (message) ->
  alert = new Backbone.Model({message: message})
  stack = $('#notification-area .alert-box').length
  html = kb.renderTemplate('shared-alert', kb.viewModel(alert), {
    afterRender: (e) ->
      setTimeout (-> $(e).animate({marginTop : '-42px'}, 600, (-> $(this).remove()))), 2000 * (stack + 1)
      #$('#notification-area').foundation()
  })

  $('#notification-area').append html