# NOTE: animations are disabled for better performance!

#= require src/view_models/home/index
#= require src/view_models/home/settings
#= require src/view_models/search/index
#= require src/view_models/sessions/new

class window.Router extends ohko.Router
  routes:
    'home'            : 'home_index'
    'login'           : 'sessions_new'
    'signout'         : 'sessions_destroy'
    'search'          : 'search_index'
    'settings'        : 'home_settings'
    
    '*default'        : 'home_index'

  # Override default config
  skip_login_for: ['sessions_new', 'users_new', 'passwords_new', 'passwords_show']
  skip_activation_for: ['all']
  redirect_if_logged_in_for: ['sessions_new', 'users_new', 'passwords_new', 'passwords_show']
  redirect_if_active_for: []
  cached_templates: ['home_index', 'search_index']

  options:
    login_action: 'sessions_new'
    #activation_action: ''
    home_action: 'home_index'
    login_path: 'login'
    #activation_path: ''
    home_path: 'home'
    logout_path: 'signout'
    
  home_index: ->
    if @needs_render_template
      vm = new HomeIndexViewModel()
      @html = kb.renderTemplate('home-index', vm)

  search_index: ->
    if @needs_render_template
      window.search_vm ||= new SearchIndexViewModel()
      @html = kb.renderTemplate('search-index', search_vm)
    else
      window.search_vm.afterRender()
 
    @keep_search_bar = true

  home_settings: ->
    vm = new HomeSettingsViewModel()
    @html = kb.renderTemplate('home-settings', vm)

  sessions_new: (url) ->
    @template_cache = {}
    vm = new SessionNewViewModel(url)
    @html = kb.renderTemplate('sessions-new', vm)

  users_new: ->
    # vm = new UserNewViewModel()
    # @html = kb.renderTemplate('users-new', vm)

  confirmations_new: ->
    # @template_cache = {}
    # vm = new ConfirmationNewViewModel()
    # @html = kb.renderTemplate('confirmations-new', vm)

  passwords_new: ->
    # vm = new PasswordNewViewModel()
    # @html = kb.renderTemplate('passwords-new', vm)

  passwords_show: ->
    # @html = kb.renderTemplate('passwords-show', kb.viewModel)

  sessions_destroy: ->
    @logout()
  
  # Special callbacks
  onBackKey: ->
    will_go_back = true

    # Handle back button interceptions
    if $('body').hasClass 'pushy-active'
      will_go_back = false
      togglePushy()

    if will_go_back
      window.history.back()
      return true      
    else
      event.preventDefault()
      return false