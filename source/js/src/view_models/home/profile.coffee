class window.HomeProfileViewModel extends ohko.PageViewModel
  constructor: ->
    @user = current_user
    @user.reset_errors()
    
    # Functionality setup
    @is_authenticable()

    # Observable attributes
    bind_attributes from: @user, to: this
    bind_errors_for this
    @show_password_fields = true
    @career_list = kb.collectionObservable(careers)
    @remaining_characters = ko.computed => 200 - @bio()?.length

  # Events
  afterRender: ->
    setTimeout (=>
      #$(document).on 'click', '#profile .buttons-check *', => @user.save_model(progress: true) TODO: bugged
    ), 0

  onSaveProfile: ->
    @user.reset_errors()

    # Send ajax request
    @user.save_model
      progress: true
      success_message: 'Cambios guardados correctamente.'
      success: =>
        @user.set 'current_password', ''
        @user.set 'password', ''
        @user.set 'password_confirmation', ''