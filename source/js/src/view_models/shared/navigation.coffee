class window.NavigationViewModel extends ohko.NavigationViewModel
  onSearch: ->
    if ohko.app.navigation?.get('searching')
      search_vm.onSearch $('#search').val()
    else
      ohko.app.router?.navigate 'search', trigger: true

  onKeyPress: (d, e) ->
    if e.keyCode is 13
      @onSearch()
      $('.search .edit').blur()

    # Allow default action
    true