class window.SessionNewViewModel extends ohko.PageViewModel
  constructor: (url) ->
    super()
    @redirect_url = decodeURIComponent(url) if url?

    # Functionality setup
    @is_authenticable()

    # Observable attributes
    @bind ['username', 'password', 'response_errors'], from: ohko.app.session
    @bind ['saving'], from: ohko.app.session, namespace: 'session'
    @bind_errors_for ohko.app.session, namespace: 'session'
    
    # Reset previous errors in case they exist
    ohko.app.session.reset_errors()

  # Events
  onLogin: ->
    if !!@username() and !!@password()
      ohko.app.session.login
        delay: 1000
        nprogress: false
        after: =>
          if ohko.app.session.get('authenticated')
            setTimeout (=> 
              ohko.app.router?.navigate @redirect_url ? '#home', trigger: true
              ohko.app.session.set('password', '')
            ), 3000

  onKeyPress: (d, e) ->
    if e.keyCode is 13
      $('.edit').blur()
      @onLogin()


    # Allow default action
    true