#= require src/models/search

class window.SearchIndexViewModel extends ohko.PageViewModel
  constructor: ->
    super()
    window.typeahead_initialized ||= false
    @search = new SearchList()

    # Observable attributes
    @indexes @search.fullCollection, collection: 'results', view_model: ohko.ViewModel #TODO: replace with custom vm
    @loading = ko.observable(false)
    @has_searched = ko.observable(false)
    @is_blank = ko.computed => @results_empty() and @has_searched()
    
  afterRender: ->
    setTimeout (=>
      ohko.app.navigation?.set 'searching', true
      setTimeout (=>
        $('.search .edit').focus() if @results_empty()

        # Initialize typeahead
        @initialize_typeahead() if not window.typeahead_initialized
      ), 300
    ), 0

  initialize_typeahead: ->
    engine = new Bloodhound
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value')
      queryTokenizer: Bloodhound.tokenizers.whitespace
      remote: ohko.app.server_api('search/autocomplete?query=%QUERY')
     
    engine.initialize()
     
    $('.typeahead').typeahead {
      minLength: 1
    }, {
      displayKey: 'value'
      name: 'search'
      source: engine.ttAdapter()
    }

    window.typeahead_initialized = true

  onSearch: (search_text) ->
    @search.fullCollection.reset()
    @search.reset()
    @search.query = search_text
    @search.links = {1: @search.url()}
    @has_searched(false)
    @loading(true) if @results_empty()
    @search.gotoFirstPage
      success: => @loading(false); @has_searched(true)
      error: => @loading(false); @has_searched(true)