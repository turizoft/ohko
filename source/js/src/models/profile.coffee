class window.Profile extends User
  paramRoot: 'user'
  urlRoot: server_api 'user/profile'
  url: -> @urlRoot

# Initialize model
Profile.setup()