class window.SearchList extends ohko.PageableCollection
  #TODO: replace with searched model
  model: 'ohko.Model'
  mode: 'infinite'

  url: ->
    ohko.app.server_api "search?query=#{@query}"